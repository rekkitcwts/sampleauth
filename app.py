#!flask/bin/python
from flask import Flask, jsonify
from flask.ext.httpauth import HTTPBasicAuth
from model import DBconn
import sys,flask
from pbkdf2 import crypt

app = Flask(__name__)
auth = HTTPBasicAuth()

def spcall(qry, param, commit=False):
    try:
        dbo = DBconn()
        cursor = dbo.getcursor()
        cursor.callproc(qry, param)
        res = cursor.fetchall()
        if commit:
            dbo.dbcommit()
        return res
    except:
        res = [("Error: " + str(sys.exc_info()[0]) + " " + str(sys.exc_info()[1]),)]
    return res

@auth.get_password
def getpassword(username):
    return spcall("getpassword", (username,))[0][0]


@app.route('/')
@auth.login_required
def index():
    return "Hello, World!"

@app.route('/api/register/<string:username>/<string:password>')
def registerUser(username, password):
    pwhash = crypt(password)
    res = spcall("registerUser", (username, pwhash), True)

    if 'Error' in res[0][0]:
        return jsonify({'status': 'error', 'message': res[0][0]})

    return jsonify({'status': 'ok', 'message': res[0][0]})

@app.route('/api/login/<string:username>/<string:password>')
@auth.verify_password
def loginUser(username, password):
    res = spcall("getpassword", (username,))

    if 'Error' in res[0][0]:
        return jsonify({'status': 'error', 'message': res[0][0]})

    pwhash = res[0][0]
    if pwhash == crypt(password, pwhash):
        return jsonify({'status': 'ok'})
    else:
        return jsonify({'status': 'KO'})




@app.after_request
def add_cors(resp):
    resp.headers['Access-Control-Allow-Origin'] = flask.request.headers.get('Origin', '*')
    resp.headers['Access-Control-Allow-Credentials'] = True
    resp.headers['Access-Control-Allow-Methods'] = 'POST, OPTIONS, GET, PUT, DELETE'
    resp.headers['Access-Control-Allow-Headers'] = flask.request.headers.get('Access-Control-Request-Headers',
                                                                             'Authorization')
    # set low for debugging

    if app.debug:
        resp.headers["Access-Control-Max-Age"] = '1'
    return resp

if __name__ == '__main__':
    app.run(debug=True)

