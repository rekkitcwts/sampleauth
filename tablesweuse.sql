CREATE TABLE users (
	id serial not null,
	username varchar(50) not null,
	password varchar(200) not null,
	primary key (id)
);

create or replace function registerUser(par_username text, par_passhash text) returns text as
$$
  declare
    loc_id text;
    loc_res text;
  begin
     select into loc_id id from users where username = par_username;
     if loc_id isnull then

       insert into users (username, password) values (par_username, par_passhash);
       loc_res = 'OK';

     else
       loc_res = 'USER_EXISTS';  
     end if;
     return loc_res;
  end;
$$
 language 'plpgsql';

create or replace function getpassword(par_username text) returns text as
$$
  declare
    loc_password text;
  begin
     select into loc_password password_hash from users where username = par_username;
     if loc_password isnull then
       loc_password = 'null';
     end if;
     return loc_password;
 end;
$$
 language 'plpgsql';